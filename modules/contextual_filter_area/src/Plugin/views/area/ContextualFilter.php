<?php

namespace Drupal\contextual_filter_area\Plugin\views\area;

use Drupal\views\Plugin\views\area\AreaPluginBase;

/**
 * Views area handlers. Insert a contextual filter inside of an area.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("contextual_filter")
 */
class ContextualFilter extends AreaPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    $x = 1;
    // TODO: Implement render() method.
  }

}
